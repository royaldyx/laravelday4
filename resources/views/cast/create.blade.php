@extends('layout.blank')
@section('judul')
    Halaman tambah pemain film
@endsection
@section('title')
    Cast
@endsection
@section('content')
<h2>Tambah Data</h2>
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukan Nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="umur">umur</label>
        <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan umur">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="bio">Bio</label>
        <textarea class="form-control" name="bio" id="bio" cols="30" rows="10"></textarea>
        @error('bio')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>&nbsp;&nbsp;
    <a href="/cast" class="btn btn-default my-2">kembali</a>

</form>
@endsection




